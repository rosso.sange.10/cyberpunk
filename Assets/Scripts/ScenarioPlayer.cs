﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioPlayer : MonoBehaviour
{
    public GameObject baloon;
    private int story;
    private int scenario;
    // Start is called before the first frame update
    public void startScenario()
    {

        //来客中セーブデータのシナリオが1だった場合
        //セリフ枠に物語のシナリオNo1を表示する
        story = 1;
        scenario = 1;

        //来客中セーブデータのシナリオが2だった場合
        //アイテム入力待ち画面

        //来客中セーブデータのシナリオが3だった場合
        //睡眠中

        //来客中セーブデータのシナリオが4だった場合
        //物語.帰宅シナリオを表示
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Uketuke")
        {
            baloon.SetActive(true);
            baloon.GetComponent<BaloonShoewr>().showScenario(story,scenario);
        }
    }
}
