﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactorMover : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector2 direction = new Vector2(h, v).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * 1f;
    }
}
