﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnBoot : MonoBehaviour
{
    public GameObject customer;
    private customerInShop customerinshop;
    // Start is called before the first frame update
    void Start()
    {
        string isCustomerComing;
        isCustomerComing = PlayerPrefs.GetString("来店中客");
        customerinshop = JsonUtility.FromJson<customerInShop>(isCustomerComing);
        if(customerinshop == null || customerinshop.customerName == null)
        {
            //新たに客を来させる
            customer.SetActive(true);

            //来客中セーブデータに顧客セーブデータを移す

            customer.GetComponent<ScenarioPlayer>().startScenario();
        }
        {
            //状況に応じて来てる客を描写する
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
