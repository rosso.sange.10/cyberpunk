﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Customer : MonoBehaviour
{
    //キャラクター,現在の物語,現在のシナリオ
    public string name;
    public int story;
    public int scenario;
}
