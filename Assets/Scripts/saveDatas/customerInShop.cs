﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class customerInShop : MonoBehaviour
{
    // 来客中の客の名前
    public string customerName;
    // 最後に再生したシナリオ番号
    public int lastPlayedScenario;
    // 現在の物語番号
    public int storyProgress;
    // 睡眠開始時間
    public int startSleepTime;
    // 睡眠時間（秒）
    public int SleepTime;
}
