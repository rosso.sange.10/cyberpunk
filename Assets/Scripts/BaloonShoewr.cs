﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BaloonShoewr : MonoBehaviour
{
    TextAsset csvFile; // CSVファイル
    List<string[]> csvDatas = new List<string[]>(); // CSVの中身を入れるリスト;
    public GameObject baloon;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Charactor")
        {
            baloon.SetActive(true);
        }
    }

    public void showScenario(int story,int scenario)
    {
        //json読み込んで表示する
        csvFile = Resources.Load("testCSV") as TextAsset; // Resouces下のCSV読み込み
        StringReader reader = new StringReader(csvFile.text);

        // , で分割しつつ一行ずつ読み込み
        // リストに追加していく
        while (reader.Peek() != -1) // reader.Peaekが-1になるまで
        {
            string line = reader.ReadLine(); // 一行ずつ読み込み
            csvDatas.Add(line.Split(',')); // , 区切りでリストに追加
        }

        // csvDatas[行][列]を指定して値を自由に取り出せる
        Debug.Log(csvDatas[story][scenario]);
    }

    public void onClick()
    {
       　//クリックしたら次を表示

        //json最後の行だったらシナリオを1個進める
        //それをキャラクター本体に通知する
    }
}
