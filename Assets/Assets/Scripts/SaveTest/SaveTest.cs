﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ItemClass a = new ItemClass();
        ItemClass b = new ItemClass();
        a.name = "aaaaa";
        a.weight = "100";
        a.number = "12";

        b.name = "bbbbb";
        b.weight = "12200";
        b.number = "13232";

        string aJson = JsonUtility.ToJson(a);
        string bJson = JsonUtility.ToJson(b);
        

        PlayerPrefs.SetString("アイテム", aJson+bJson);



        // }で区切ってそれごとにオブジェクトに値を入れておく
        // オブジェクトは配列にする
        Dictionary<string, ItemClass> dic = new Dictionary<string, ItemClass>();
        dic.Add("a", a);
        Debug.Log(dic["a"]);
    }
}
